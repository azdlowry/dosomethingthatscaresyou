using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace DoSomethingThatScaresYou.AcceptanceTests.Proxy
{
    public class RegisterPage : Page
    {
        private readonly IWebDriver webDriver;

        public RegisterPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }

        public override bool IsVisible()
        {
            return webDriver.Title.Contains("Register");
        }

        public override string GetUrl()
        {
            return BaseUrl + "Account/Register";
        }

        public void SetUsername(string username)
        {
            webDriver.FindElement(By.Id("UserName"))
                .SendKeys(username);
        }

        public void SetPassword(string password)
        {
            webDriver.FindElement(By.Id("Password"))
                .SendKeys(password);
        }

        public void SetConfirmationPassword(string password)
        {
            webDriver.FindElement(By.Id("ConfirmPassword"))
                .SendKeys(password);
        }

        public void ClickRegisterButton()
        {
            webDriver.FindElement(By.Id("ConfirmPassword"))
                .Submit();
        }
    }
}
