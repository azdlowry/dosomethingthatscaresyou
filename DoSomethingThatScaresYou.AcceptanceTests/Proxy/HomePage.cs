using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace DoSomethingThatScaresYou.AcceptanceTests.Proxy
{
    public class HomePage : Page
    {
        private readonly IWebDriver webDriver;

        public HomePage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
        }
        public override bool IsVisible()
        {
            return webDriver.Title.Contains("Home Page");
        }

        public override string GetUrl()
        {
            return BaseUrl;
        }

        public void ClickRegisterLink()
        {
            webDriver.FindElement(By.Id("registerLink"))
                .Click();
        }
    }
}
