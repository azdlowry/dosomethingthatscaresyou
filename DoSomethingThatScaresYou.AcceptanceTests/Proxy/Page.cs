using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoSomethingThatScaresYou.AcceptanceTests.Proxy
{
    public abstract class Page
    {
        public Page()
        {

        }

        public string BaseUrl
        {
            get
            {
                return "http://localhost:50173/";
            }
        }

        public abstract bool IsVisible();

        public abstract string GetUrl();
    }
}
