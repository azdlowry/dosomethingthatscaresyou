using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace DoSomethingThatScaresYou.AcceptanceTests.Proxy
{
    public class Website
    {
        private readonly IWebDriver webdriver;

        public Website(IWebDriver webdriver)
        {
            this.webdriver = webdriver;
        }

        public void NavigateTo(string webpageName)
        {
            webdriver.Navigate().GoToUrl(GetPage(webpageName).GetUrl());
        }

        public void Logout()
        {

        }

        public Page GetPage(string webpageName)
        {
            switch (webpageName)
            {
                case "home page":
                    return new HomePage(webdriver);
                case "register":
                    return new RegisterPage(webdriver);
                default:
                    throw new ArgumentException("Unknown page name", webpageName);
            }
        }

        public TPage GetPage<TPage>() where TPage : Page
        {
            return (TPage)typeof(TPage)
                .GetConstructor(new[] { typeof(IWebDriver) })
                .Invoke(new[] { webdriver });
        }
    }
}
