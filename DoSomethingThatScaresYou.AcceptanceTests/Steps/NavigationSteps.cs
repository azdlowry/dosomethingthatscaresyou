using System;
using DoSomethingThatScaresYou.AcceptanceTests.Proxy;
using TechTalk.SpecFlow;
using Xunit;

namespace DoSomethingThatScaresYou.AcceptanceTests.Steps
{
    [Binding]
    public class NavigationSteps
    {
        private readonly Website website;

        public NavigationSteps(Website website)
        {
            this.website = website;
        }

        [Given(@"I am on the '(.*)' page")]
        public void GivenIAmOnThePage(string webpageName)
        {
            website.NavigateTo(webpageName);
        }

        [When(@"I visit the '(.*)' page")]
        public void WhenIVisitThePage(string webpageName)
        {
            website.NavigateTo(webpageName);
        }

        [Then(@"I should see the '(.*)' page")]
        public void ThenIShouldSeeThe(string webpageName)
        {
            var requestedPage = website.GetPage(webpageName);
            Assert.True(requestedPage.IsVisible());
        }
    }
}
