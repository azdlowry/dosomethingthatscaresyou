using System;
using BoDi;
using DoSomethingThatScaresYou.AcceptanceTests.Proxy;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using TechTalk.SpecFlow;
using Xunit;

namespace DoSomethingThatScaresYou.AcceptanceTests.Steps
{
    [Binding]
    public class WebDriverSupport
    {
        private readonly IObjectContainer objectContainer;

        public WebDriverSupport(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void InitializeWebDriver()
        {
            var webDriver = new FirefoxDriver();
            webDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            objectContainer.RegisterInstanceAs<IWebDriver>(webDriver);
        }
    }
}
