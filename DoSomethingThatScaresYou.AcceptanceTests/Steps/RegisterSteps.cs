﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoSomethingThatScaresYou.AcceptanceTests.Proxy;
using TechTalk.SpecFlow;

namespace DoSomethingThatScaresYou.AcceptanceTests.Steps
{
    [Binding]
    public class RegisterSteps
    {
        private readonly Website website;

        public RegisterSteps(Website website)
        {
            this.website = website;
        }

        [When(@"I fill in the username with '(.*)'")]
        public void WhenIFillInTheUsernameWith(string username)
        {
            website.GetPage<RegisterPage>()
                .SetUsername(username);
        }

        [When(@"I fill in the password with '(.*)'")]
        public void WhenIFillInThePasswordWith(string password)
        {
            website.GetPage<RegisterPage>()
                .SetPassword(password);
        }

        [When(@"I fill in the confirmation password with '(.*)'")]
        public void WhenIFillInTheConfirmationPasswordWith(string password)
        {
            website.GetPage<RegisterPage>()
                .SetConfirmationPassword(password);
        }

        [When(@"I click the register button")]
        public void WhenIClickTheRegisterButton()
        {
            website.GetPage<RegisterPage>()
                .ClickRegisterButton();
        }
    }
}
