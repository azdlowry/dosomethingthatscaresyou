using System;
using DoSomethingThatScaresYou.AcceptanceTests.Proxy;
using TechTalk.SpecFlow;
using Xunit;

namespace DoSomethingThatScaresYou.AcceptanceTests.Steps
{
    [Binding]
    public class UserSteps
    {
        private readonly Website website;

        public UserSteps(Website website)
        {
            this.website = website;
        }

        [Given(@"I am a new user")]
        public void GivenIAmANewUser()
        {
            website.Logout();
        }
    }
}
