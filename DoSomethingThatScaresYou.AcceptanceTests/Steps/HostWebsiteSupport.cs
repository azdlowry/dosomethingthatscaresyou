using System;
using System.IO;
using System.Runtime.CompilerServices;
using BoDi;
using DoSomethingThatScaresYou.AcceptanceTests.Proxy;
using IISExpressAutomation;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using Xunit;

namespace DoSomethingThatScaresYou.AcceptanceTests.Steps
{
    [Binding]
    public class HostWebsiteSupport
    {
        private static IISExpress iis;

        [BeforeTestRun]
        public static void StartWebsite()
        {
            iis = new IISExpress(new Parameters
            {
                Path = GetWebsiteFolderPath(),
                Port = 50173
            });
        }

        [AfterTestRun]
        public static void TearDownWebsite()
        {
            iis.Dispose();
        }

        private static string GetWebsiteFolderPath([CallerFilePath]string filenameOfThisClass = "")
        {
            var stepsFolder = Path.GetDirectoryName(filenameOfThisClass);
            var acceptanceTestFolder = Path.GetDirectoryName(stepsFolder);
            var solutionFolder = Path.GetDirectoryName(acceptanceTestFolder);
            return Path.Combine(solutionFolder, "DoSomethingThatScaresYou");
        }
    }
}
