﻿using System;
using DoSomethingThatScaresYou.AcceptanceTests.Proxy;
using TechTalk.SpecFlow;
using Xunit;

namespace DoSomethingThatScaresYou.AcceptanceTests.Steps
{
    [Binding]
    public class HomePageSteps
    {
        private readonly Website website;

        public HomePageSteps(Website website)
        {
            this.website = website;
        }

        [When(@"I click the register link")]
        public void WhenIClickTheRegisterLink()
        {
            website.GetPage<HomePage>()
                .ClickRegisterLink();
        }
    }
}
