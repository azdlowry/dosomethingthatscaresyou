﻿Feature: HomePage
	In order become a happier person
	As a normal person
	I want to do something that scares me
	
Scenario: Visit the home page
	Given I am a new user
	When I visit the 'home page' page
	Then I should see the 'home page' page

Scenario: Register new user
	Given I am a new user
	And I am on the 'home page' page
	When I click the register link
	Then I should see the 'register' page
