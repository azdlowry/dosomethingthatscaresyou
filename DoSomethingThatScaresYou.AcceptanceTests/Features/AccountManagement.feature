﻿Feature: AccountManagement
	In order to securely use the web site
	As a normal person
	I want to manage my account details

Scenario: Register User
	Given I am on the 'register' page
	When I fill in the username with 'Alice'
	And I fill in the password with 'password'
	And I fill in the confirmation password with 'password'
	And I click the register button
	Then I should see the 'home page' page
	And I should be logged in as 'Alice'
